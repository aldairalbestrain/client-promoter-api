const { response } = require('express');
require('./../db/connection');
const path = require('path');

//const find = require('../db/crud/find');
const findOne = require('../db/crud/findOne');
const remove = require('../db/crud/remove');
const save = require('../db/crud/save');
const update = require('../db/crud/Update');
const Prospecto = require('../schemas/prospectos.schema.js');

const prospectosGetList = async (req, res = response) => {
    const { limit = 10, page = 0 } = req.query;

    const [ total, prospectos ] = await Promise.all([
        Prospecto.countDocuments(),
        Prospecto.find()
            .skip( Number((page - 1) * limit) )
            .limit( Number(limit) )
    ]);

    res.status(200).json({
        total: Math.ceil(total/limit),
        prospectos
    });

}

const prospectosGet = async (req, res = response) => {

    const prospect = await Prospecto.findById(req.query.id)
    console.log(prospect)

    res.status(200).json({
        prospect
    });

}

const prospectosPost = async (req, res = response) => {
    try {
        const { ...prospectoInfoRequest } = req.body;


        const documents = req.files ? Object.keys(req.files).map(file => {
            const fileExtension = path.extname(req.files[file].name)
            return '/uploads/' + file + fileExtension
        }) : []

        //save prospecto info
        const prospecto = new Prospecto({
            ...prospectoInfoRequest,
            documents
        });

        await save(prospecto);

        SaveDocuments(req, res);

        res
            .status(200)
            .json({
                message: 'El prospecto ha sido creado'
            });

    } catch (error) {
        res.status(500).json({
            message: error.message
        });
    }

}


const SaveDocuments = async (req, res) => {

    if (!req.files || Object.keys(req.files).length === 0) {
        return false;
    }

    for (var i in req.files) {

        const fileExtension = path.extname(req.files[i].name)
        const uploadPath = path.join(__dirname, '../uploads/' + i + fileExtension);

        await req.files[i].mv(uploadPath, (err) => {
            if (err) {
                return res.status(500).json({
                    message: ['Ocurrio un error al guardar los documentos']
                });
            }
        })
    }

}

const prospectosPut = async (req, res = response) => {

    try {
        const _id = req.query.id;

        await validarIdDeProspecto(_id, 'editar');

        await update(Prospecto, { _id }, req.body);

        res
            .status(200)
            .json({
                'message': 'La prospecto se ha editado correctamente'
            });

    } catch (error) {

        res.status(500).json({
            message: error.message
        });
    }

}

const prospectosCambioDeEstado = async (req, res = response) => {

    try {

        const _id = req.query.id;

        await validarIdDeProspecto(_id, 'editar');

        if (req.body.status === 'enviado' || req.body.status === 'aceptado' || req.body.status === 'rechazado') {
            await update(Prospecto, { _id }, { status: req.body.status, observaciones: req.body.observaciones });

            res
                .status(200)
                .json({
                    message: 'Se ha cambiado el estado de la prospecto correctamente'
                });
        } else {
            throw new Error('No se ha podido cambiar el estado de la prospecto');
        }

    } catch (error) {

        res.status(500).json({
            message: error.message
        });

    }

}

const validarIdDeProspecto = async (_id, accion) => {

    if (_id) {

        await findOne(Prospecto, _id)
            .then(response => {

                if (!response) throw new Error(`La prospecto que desea ${accion} no ha existe`);

            })
            .catch(error => {
                throw new Error(`La prospecto que desea ${accion} no ha existe`);
            });

    } else {
        throw new Error(`Se requiere el id de la prospecto que desea ${accion}`);
    }

}

const prospectosDelete = async (req, res = response) => {

    try {

        const _id = req.query.id;

        await validarIdDeProspecto(_id, 'eliminar');

        await remove(Prospecto, { _id });

        res
            .status(200)
            .json({
                message: 'La prospecto se ha eliminado correctamente'
            });

    } catch (error) {

        res.status(500).json({
            message: error.message
        });

    }

}

module.exports = {
    prospectosGet,
    prospectosGetList,
    prospectosPost,
    prospectosPut,
    prospectosCambioDeEstado,
    prospectosDelete
}