const { response } = require('express');
const Usuario = require( '../schemas/usuario.schema' );
const bcryptjs = require('bcryptjs');
const { generarJWT } = require('../helpers/generar-jwt');

const login = async ( req, res = response ) => {

    const { correo, password } = req.body;

    try {
        
        const usuario = await Usuario.find({ correo });

        if ( usuario.length <= 0 ) {
            return res.status(400).json({
                message: ['El correo y la contraseña son incorrectos']
            });
        }

        if ( !usuario[0].estado ) {
            return res.status(400).json({//nesta en estado falso
                message: ['El correo y la contraseña son incorrectos']
            });
        }

        const validPassword = bcryptjs.compareSync( password, usuario[0].password );
        if ( !validPassword ) {
            return res.status(400).json({//validacion de la contraseña
                message: ['El correo y la contraseña son incorrectos']
            });
        }

        //generar jwt
        console.log( usuario[0].rol );
        const token  = await generarJWT( usuario[0].id, usuario[0].rol );

        res.status(200).json({
            message: 'Autenticación correcta',
            token
        });

    } catch (error) {
        return res.status(500).json({
            message: ['Algo salio mal con el administrador']
        });
    }

}

module.exports = {
    login
}