const { response } = require('express');
require('./../db/connection');
const bcryptjs = require('bcryptjs');

const save = require('../db/crud/save');
const Usuario = require('../schemas/usuario.schema');

const usuariosGet = async ( req, res = response ) => {

    const { limite = 10, desde = 0 } = req.query;
    const query = { estado: true, rol: 'ADMIN_ROL' };

    const [ total, usuarios ] = await Promise.all([
        Usuario.countDocuments( query ),
        Usuario.find( query )
            .skip( Number(desde) )
            .limit( Number(limite) )
    ]);

    const usuariosFilter = [];
    usuarios.map( ( usuario , i ) => {
        
        const { password, _id, nombre, correo } = usuario;
        usuariosFilter.push( {
            nombre,
            correo,
            id: _id
        });
    });

    res.status(200).json({
        total,
        usuariosFilter
    });

}

const usuariosPost = async (req, res = response) => {

    const { nombre, correo, password, rol } = req.body;
    const usuario = new Usuario({
        nombre, 
        correo, 
        password, 
        rol
    });

    //encriptar la contra
    const salt = bcryptjs.genSaltSync();
    usuario.password = bcryptjs.hashSync( password, salt );

        await save( usuario )
        
        res.status(200).json({
            message: 'Se ha creado el usuario correctamente'
        });

    
}

module.exports = {
    usuariosGet,
    usuariosPost
}