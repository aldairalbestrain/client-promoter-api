const { validationResult } = require("express-validator");

const validarCampos = ( req, res, next ) => {

    const errors = validationResult( req );
    if ( !errors.isEmpty() ) {
        //HACER UN ARRAY DE ERRORES Y ENVIARLOS
        const errorsMessage = [];
        errors.errors.map( error => {
            errorsMessage.push( error.msg );
        });

        console.log( errors )
        return res.status(400).json({
            message:  errorsMessage
        });
    }

    next();
}

module.exports = {
    validarCampos
}