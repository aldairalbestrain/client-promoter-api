const { response } = require("express");

const esSuperAdminRol = ( req, res = response, next ) => {

    if ( !req.usuario ) {
        return res.status(500).json({
            message: ['Se quiere verificar el rol sin validar el token primero']
        });
    }

    const { rol, nombre } = req.usuario;

    if ( rol !== 'SUPER_ADMIN_ROL' ) {
        return res.status(401).json({
            message: [`${ nombre } no tiene permisos para realizar esta acción`]
        });
    }

    next();
}

const esAdminRol = ( req, res = response, next ) => {

    if ( !req.usuario ) {
        return res.status(500).json({
            message: ['Se quiere verificar el rol sin validar el token primero']
        });
    }

    const { rol, nombre } = req.usuario;

    if ( rol !== 'ADMIN_ROL' ) {
        return res.status(401).json({
            message: [`${ nombre } no tiene permisos para realizar esta acción`]
        });
    }

    next();
}


const tieneRol = ( ...roles ) => {

    return ( req, res = response, next ) => {

        if ( !req.usuario ) {
            return res.status(500).json({
                message: ['Se quiere verificar el rol sin validar el token primero']
            });
        }

        if ( !roles.includes( req.usuario.rol ) ) {
            return res.status(401).json({
                message: ['El usuario no tiene permiso de realizar esta acción']
            });
        }

        next();
    }
}

module.exports = {
    esSuperAdminRol,
    esAdminRol,
    tieneRol
}