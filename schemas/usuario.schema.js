const { Schema, model } = require('mongoose');

const UsuarioSchema = Schema({
    nombre: {
        type: String,
        require: [true, 'El nombre es obligatorio']
    },
    correo: {
        type: String,
        require: [true, 'El correo es obligatorio'],
        unique: true
    },  
    password: {
        type: String,
        require: [true, 'La contraseña es obligatoria']
    },
    rol: {
        type: String,
        require: true,
        emun: ['SUPER_ADMIN_ROL', 'ADMIN_ROL']
    }, 
    estado: {
        type: Boolean,
        default: true
    }
});

module.exports = model( 'Usuario', UsuarioSchema );