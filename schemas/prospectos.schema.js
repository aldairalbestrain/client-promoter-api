const { response } = require('express');
const { Schema, model } = require('mongoose');
require('../db/connection');


const prospecto = new Schema({
    nombre: String,
    apellidoPaterno: String,
    apellidoMaterno: String,
    calle: String,
    numeroDeCalle: String,
    colonia: String,
    codigoPostal: String,
    telefono: String,
    rfc: String,
    documents: [String],
    status: {
        type: String,
        default: "enviado"
    },
    observaciones: {
        type: String,
        default: ""
    }
});

module.exports = model('prospecto', prospecto);