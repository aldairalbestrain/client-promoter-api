const Role = require('../schemas/role.schema');
const Usuario = require('../schemas/usuario.schema');

const esRolValido = async ( rol = '' ) => {

    const existeRol = await Role.findOne({ rol });
    if ( !existeRol ) {
        throw new Error(`El rol ${ rol } no está registrado en la base de datos`)
    }

}

const correoExiste = async ( correo = '' ) => {
    const existeEmail = await Usuario.findOne({ correo });
    if ( existeEmail ) {
        throw new Error( 'El correo ya existe' );
    }

}

//validar ids

module.exports = {
    esRolValido,
    correoExiste
}