const { uploadsGet } = require('../controllers/uploads.controllers');
const path = require('path');

const { Router } = require('express');
const router = Router();

router.get( '/:img', (req, res) => {
    
    res.sendFile( path.join( __dirname, '../uploads/' + req.params.img ) );
} );

module.exports = router;
