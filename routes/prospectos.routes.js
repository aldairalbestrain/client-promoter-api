const { 
    prospectosGet,
    prospectosGetList,
    prospectosPost, 
    prospectosPut, 
    prospectosCambioDeEstado,
    prospectosDelete
} = require('../controllers/prospectos.controller');

const { Router } = require('express');
const { validarJWT } = require('../middlewares/validar-jwt');
const { esSuperAdminRol } = require('../middlewares/validar-roles');
const { tieneRol } = require('../middlewares/validar-roles');

const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');

const router = Router();

router.get( '/list', [
    validarJWT,
    tieneRol( 'SUPER_ADMIN_ROL', 'ADMIN_ROL' )
],prospectosGetList);

router.get( '/', [
    validarJWT,
    tieneRol( 'SUPER_ADMIN_ROL', 'ADMIN_ROL' )
], prospectosGet);

router.post( '/',[
    validarJWT,
    esSuperAdminRol,
    check( 'nombre', 'El nombre es obligatorio' ).not().isEmpty(),
    check( 'apellidoPaterno', 'El apellido paterno es obligatorio' ).not().isEmpty(),
    check( 'calle', 'La calle es obligatoria' ).not().isEmpty(),
    check( 'numeroDeCalle', 'El número de la calle es obligatorio' ).not().isEmpty(),
    check( 'colonia', 'El nombre de la colonia es obligatorio' ).not().isEmpty(),
    check( 'codigoPostal', 'El codigo postal es obligatorio' ).not().isEmpty(),
    check( 'telefono', 'El numero de telefono es obligatorio' ).not().isEmpty(),
    check( 'rfc', 'El RFC es obligatorio' ).not().isEmpty(),
    validarCampos    
], prospectosPost );

router.put( '/', [
    validarJWT,
    tieneRol( 'SUPER_ADMIN_ROL', 'ADMIN_ROL' ),
    check( 'nombre', 'El nombre es obligatorio' ).not().isEmpty(),
    check( 'apellidoPaterno', 'El apellido paterno es obligatorio' ).not().isEmpty(),
    check( 'calle', 'La calle es obligatoria' ).not().isEmpty(),
    check( 'numeroDeCalle', 'El número de la calle es obligatorio' ).not().isEmpty(),
    check( 'colonia', 'El nombre de la colonia es obligatorio' ).not().isEmpty(),
    check( 'codigoPostal', 'El codigo postal es obligatorio' ).not().isEmpty(),
    check( 'nombre', 'El numero de telefono es obligatorio' ).not().isEmpty(),
    check( 'rfc', 'El RFC es obligatorio' ).not().isEmpty(),
    validarCampos 
], prospectosPut );

router.patch( '/', [
    validarJWT,
    tieneRol( 'SUPER_ADMIN_ROL', 'ADMIN_ROL' ),
    check( 'observaciones', 'Es requerido agregar observaciones' ).not().isEmpty(),
    validarCampos
], prospectosCambioDeEstado );

router.delete( '/', [
    validarJWT,
    tieneRol( 'SUPER_ADMIN_ROL', 'ADMIN_ROL' )
], prospectosDelete );

module.exports = router;