const { 
    usuariosGet,
    usuariosPost
 } = require('../controllers/users.controllers');

const { Router } = require('express');
const { check } = require('express-validator');

const { validarCampos } = require('../middlewares/validar-campos');

const { 
    esRolValido, 
    correoExiste 
} = require('../helpers/db-validators');
const router = Router();

const { validarJWT } = require('../middlewares/validar-jwt');
const { esSuperAdminRol } = require('../middlewares/validar-roles');

router.get( '/', [
    validarJWT,
    esSuperAdminRol,
], usuariosGet );

router.post( '/', [
    validarJWT,
    esSuperAdminRol,
    check( 'nombre', 'El nombre es obligatorio' ).not().isEmpty(),
    check( 'password', 'La contraseña debe de ser más de 6 caracteres' ).isLength({ min: 6 }),
    check( 'correo', 'El correo no es válido' ).isEmail(),
    check( 'correo' ).custom( correoExiste ),
    //check( 'rol', 'No es un rol válido' ).isIn([ 'SUPER_ADMIN_ROL', 'ADMIN_ROL' ]),
    check('rol').custom( esRolValido ),
    validarCampos    
], usuariosPost );

module.exports = router;