const mongoose = require('mongoose');

const url = 'mongodb+srv://albestrain:albestrain@cluster0.bbmtu.mongodb.net/prospectosdb';
const db = mongoose.connection;

mongoose.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .catch(err => console.log(err));


db.once('open', _ => {
    console.log('Database is connected to', url);
});


db.on('error', err => {
    console.log(err);
})