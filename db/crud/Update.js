require('../connection');

const update = async ( schema, where, newData ) => {
    return await schema.findOneAndUpdate( where, newData );
}

module.exports = update;