require('../connection');

const remove = async ( schema, where ) => {
    //return await schema.findById( where  ).remove().exec();
    return await schema.find( where  ).remove().exec();
}

module.exports = remove;