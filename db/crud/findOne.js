require('../connection');

const findOne = async ( schema, id ) => {
    return await schema.findById( id  );
}

module.exports = findOne;