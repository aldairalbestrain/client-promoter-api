require('../connection');

const find = async ( schema, where ) => {
    return await schema.find( where  );
}

module.exports = find;