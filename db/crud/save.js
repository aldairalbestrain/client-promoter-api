require('../connection');

const save = async ( schema ) => {
    return await schema.save();
}

module.exports = save;