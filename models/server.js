const express = require('express');
const cors = require('cors');
const bp = require('body-parser');
var multer = require('multer');
var upload = multer();
const fileUpload = require('express-fileupload');

require('colors');

class Server {

    constructor() {
        this.app = express();
        this.port = process.env.PORT;

        //Routes
        this.usuariosRoutePath = '/api/usuarios';
        this.authPath = '/api/auth';
        this.prospectosRoutePath = '/api/prospectos';
        this.uploadsRoutePath = '/api/uploads';

        //middelwares
        this.middlewares();

        this.router();
    }

    middlewares() {
        //CORS
        this.app.use( cors() );

        this.app.use(bp.json())
        this.app.use(bp.json({
            verify: (req, res, buf) => {
              req.rawBody = buf
            }
          }))

          //file upload
          this.app.use(fileUpload({
            useTempFiles : true,
            tempFileDir : '/tmp/'
            }));
        
        //directorio publico
        this.app.use( express.static('public') ); 
    }

    router() {
        this.app.use( this.authPath,  require( '../routes/auth.routes.js' ) );
        this.app.use( this.usuariosRoutePath,  require( '../routes/user.routes' ) );
        this.app.use( this.prospectosRoutePath, require( '../routes/prospectos.routes' ) );
        this.app.use( this.uploadsRoutePath, require( '../routes/uploads.routes' ) );
    }

    listen() {
        this.app.listen(this.port, () => {
            console.log('Servidor corriendo en el puerto'.yellow, this.port.blue);
        });
    }
}

module.exports = Server;